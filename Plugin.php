<?php

namespace Omidmm\BlogTimeRead;

use Event;
use RainLab\Blog\Models\Post;
use System\Classes\PluginManager;
use System\Classes\PluginBase;
use RainLab\Blog\Models\Post as PostModel;
use Omidmm\BlogTimeRead\Models\Settings;
use Omidmm\BlogTimeRead\Helpers\TimeRead;
use Omidmm\BlogTimeRead\Components\TimeRead as TimeReadComponent;

/**
 * Class Plugin
 *
 * @package Omidmm\BlogTimeRead
 */
class Plugin extends PluginBase
{
    const DEFAULT_ICON = 'icon-clock-o';

    const LOCALIZATION_KEY = 'omidmm.blogtimeread::lang.';

    /**
     * @var array   Require some plugins
     */
    public $require = [
        'RainLab.Blog',
        'RainLab.Translate'
    ];

    /** @var TimeRead  */
    private $helper;

    /**
     * Returns information about this plugin
     *
     * @return  array
     */
    public function pluginDetails()
    {
        return [
            'name'        => self::LOCALIZATION_KEY . 'plugin.name',
            'description' => self::LOCALIZATION_KEY . 'plugin.description',
            'author'      => 'Siarhei <Gino Pane> Karavai',
            'icon'        => self::DEFAULT_ICON,
            'homepage'    => 'https://github.com/GinoPane/oc-blogtimetoread-plugin'
        ];
    }

    /**
     * Boot method, called right before the request route
     */
    public function boot()
    {
        $this->helper = new TimeRead(Settings::instance());

        // extend the post model
        $this->extendModel();
    }

    /**
     * Register components
     *
     * @return  array
     */
    public function registerComponents()
    {
        return [
            TimeReadComponent::class => TimeReadComponent::NAME,
        ];
    }

    /**
     * Register plugin settings
     *
     * @return array
     */
    public function registerSettings(){
        return [
            'settings' => [
                'label'       => self::LOCALIZATION_KEY . 'settings.name',
                'description' => self::LOCALIZATION_KEY . 'settings.description',
                'icon'        => self::DEFAULT_ICON,
                'class'       => Settings::class,
                'order'       => 800,
                'category'    => 'rainlab.blog::lang.blog.menu_label'
            ]
        ];
    }

    /**
     * @return array
     */
    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'timeToRead' => [$this, 'getTimeRead']
            ]
        ];
    }

    /**
     * @param      $text
     * @param null $speed
     * @param null $roundingEnabled
     *
     * @return int
     */
    public function getTimeRead($text, $speed = null, $roundingEnabled = null)
    {
        return $this->helper->calculate(
            $text,
            array_filter([
                Settings::READING_SPEED_KEY => $speed,
                Settings::ROUNDING_UP_ENABLED_KEY => $roundingEnabled
            ])
        );
    }

    /**
     * Extend RainLab Post model
     */
    private function extendModel()
    {

        PostModel::extend(function ($model) {
            $model->bindEvent('model.afterFetch', function () use ($model) {
                $model->time_read = ($model->time_read) ? $model->time_read : $this->helper->calculate($model->content);
            });
        });
        Event::listen('backend.form.extendFields', function ($form){
            if(PluginManager::instance()->hasPlugin('Rainlab.Blog') && $form->model instanceof  Post) {
                $form->addFields([
                    'time_read'=>[
                        'label'=> 'omidmm.blogtimeread::lang.blog.timeToRead.title',
                        'description'=> 'omidmm.blogteimeread::lang.blog.timeToRead.decrtiption',
                        'comment'=>'omidmm.blogtimeread::lang.blog.timeToRead.comment',
                        'type'=>'number',
                        'size'    => 'tiny',
                        'span'=>'left',
                        'default'=> 3,
                        'tab'     => 'SEO'
                    ]
                ],'secondary');
            }
        });
    }
}
