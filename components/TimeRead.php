<?php

namespace Omidmm\BlogTimeRead\Components;

use RainLab\Blog\Models\Post;
use Cms\Classes\ComponentBase;
use Omidmm\BlogTimeRead\Plugin;
use Omidmm\BlogTimeRead\Models\Settings;
use Omidmm\BlogTimeRead\Helpers\TimeRead as TimeReadHelper;


/**
 * Class TimeRead
 *
 * @package Omidmm\BlogTaxonomy\Components
 */
class TimeRead extends ComponentBase
{
    const NAME = 'timeRead';

    /**
     * @var int
     */
    private $readingSpeed;

    /**
     * @var bool
     */
    private $isRoundingUpEnabled;

    /**
     * @var string
     */
    private $postSlug;

    /**
     * @var int
     */
    public $minutes;

    /**
     * Returns information about this component, including name and description.
     */
    public function componentDetails()
    {
        return [
            'name'        => Plugin::LOCALIZATION_KEY . 'components.timeread.name',
            'description' => Plugin::LOCALIZATION_KEY . 'components.timeread.description'
        ];
    }

    /**
     * Component Properties
     * @return array
     */
    public function defineProperties()
    {
        return [
            'postSlug' => [
                'title'       => Plugin::LOCALIZATION_KEY . 'components.timeread.post_slug_title',
                'description' => Plugin::LOCALIZATION_KEY . 'components.timeread.post_slug_description',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ],

            'readingSpeed' => [
                'title'         => Plugin::LOCALIZATION_KEY . 'settings.default_reading_speed',
                'description'   => Plugin::LOCALIZATION_KEY . 'settings.default_reading_speed_comment',
                'default'       => Settings::DEFAULT_READING_SPEED,
                'type'          => 'string',
                'validationPattern' => '^(0+)?[1-9]\d*$',
                'showExternalParam' => false
            ],

            'isRoundingUpEnabled' => [
                'title'       =>    Plugin::LOCALIZATION_KEY . 'settings.rounding_up_enabled',
                'description' =>    Plugin::LOCALIZATION_KEY . 'settings.rounding_up_enabled_comment',
                'type'        =>    'checkbox',
                'default'     =>    Settings::DEFAULT_ROUNDING_UP_ENABLED,
                'showExternalParam' => false
            ],
        ];
    }

    /**
     * Query the tag and posts belonging to it
     */
    public function onRun()
    {
        $this->prepareVars();

        $this->calculateReadingTime();


    }

    /**
     * Prepare variables
     *
     * @return void
     */
    private function prepareVars()
    {
        $this->postSlug = (string)$this->property('postSlug');
        $this->readingSpeed = (int)$this->property('readingSpeed');
        $this->isRoundingUpEnabled = (bool)$this->property('isRoundingUpEnabled');
    }

    /**
     * @return void
     */
    private function calculateReadingTime()
    {
        $post = Post::whereSlug($this->postSlug)->first();

        if (!$post) {
            $this->minutes = 0;
            return;
        }
        if($post->time_read){
            $this->minutes = $post->time_read;
        }else{
            $this->minutes = TimeReadHelper::get()->calculate(
                $post->content,
                array_filter([
                    Settings::READING_SPEED_KEY => $this->readingSpeed,
                    Settings::ROUNDING_UP_ENABLED_KEY => $this->isRoundingUpEnabled
                ])
            );
        }


    }
}
