<?php

namespace omidmm\blogtimeread\updates;

use System\Classes\PluginManager;
use Schema;

class CreateTimeRead extends \October\Rain\Database\Updates\Migration
{
    public function up()
    {
        if (PluginManager::instance()->hasPlugin('Rainlab.Blog')) {
            Schema::table('rainlab_blog_posts', function ($table) {
                $table->integer('time_read')->nullable();
            });
        }
    }

    public function down()
    {
        if (PluginManager::instance()->hasPlugin('Rainlab.Blog')) {
            Schema::table('rainlab_blog_posts', function ($table) {
                $table->dropColumn('time_read');
            });
        }
    }


}